# requires terraform 12.29

variable "web_servers" { }
variable "load_balancers" { }
variable "domain" { }

# generate web servers and load balancers

# create storage pool
resource "libvirt_pool" "demo3" {
  name = "demo3a"
  type = "dir"
  path = "${var.pool_dir}/demo3a"
}

module "demo_server_one" {
  source = "./modules/demo-server"

  #set the variables
  machine_name    = "${var.web_servers[0].name}"
  network_address = "${var.web_servers[0].address}"
  pool_name = libvirt_pool.demo3.name
  public_key = file("${var.public_key}")
  private_key = "${path.module}/../.ssh/${var.private_key}"
  domain = var.domain
  image_repo = var.image_repo
}

module "demo_server_two" {
  source = "./modules/demo-server"

  #set the variables
  machine_name    = "${var.web_servers[1].name}"
  network_address = "${var.web_servers[1].address}"
  pool_name = libvirt_pool.demo3.name
  public_key = file("${var.public_key}")
  private_key = "${path.module}/../.ssh/${var.private_key}"
  domain = var.domain
  image_repo = var.image_repo
}

module "load_balancer_one" {
  source = "./modules/load-balancer"

  #set the variables
  machine_name    = "${var.load_balancers[0].name}"
  network_address = "${var.load_balancers[0].address}"
  pool_name = libvirt_pool.demo3.name
  public_key = file("${var.public_key}")
  private_key = "${path.module}/../.ssh/${var.private_key}"
  domain = var.domain
  image_repo = var.image_repo
}
