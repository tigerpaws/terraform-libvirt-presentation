
web_servers = [
  {
    name    = "vweb1",
    address = "192.168.122.11/24"
  },
  {
    name    = "vweb2",
    address = "192.168.122.12/24"
  }
]

load_balancers = [
  {
    name    = "vlb1",
    address = "192.168.122.5/24"
  },
  {
    name    = "vlb2",
    address = "192.168.122.6/24"
  }
]
