
variable "image_repo" {
  description = "the local repo where cloud image files are stored. Typically $DEV/lib/libvirt/images"
  type        = string
}

variable "terraform_user" {
  description = " user used to connect to instances via ssh"
  default     = "tf"
}

variable "private_key" {
  description = "private key file to connect to instances with ssh"
  default     = "../.ssh/id_ed25519"
}

variable "public_key" {
  description = "public key file to be stored in ansible user's authorized_keys file"
  default     = "../.ssh/id_ed25519.pub"
}
