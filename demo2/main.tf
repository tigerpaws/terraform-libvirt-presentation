
resource "libvirt_volume" "centos8-qcow2" {
  #  pool = "local"
  for_each = var.vms
  name     = "centos8.${each.key}.qcow2"
  #source = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
  source = "${var.image_repo}/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
  format = "qcow2"
}

data "template_file" "user_data" {
  template = "${file("${path.module}/cloud-init.cfg")}"
  for_each = var.vms
  vars = {
    hostname   = "${each.key}"
    fqdn       = "${each.key}.3sz.ca"
    public_key = file("${var.public_key}")
  }
}

# Use CloudInit to add the instance

resource "libvirt_cloudinit_disk" "commoninit" {
  for_each  = var.vms
  name      = "${each.key}.init.iso"
  user_data = data.template_file.user_data[each.key].rendered
}

# Define KVM domain to create
resource "libvirt_domain" "vms" {
  for_each = var.vms
  name     = each.key
  memory   = "2048"
  vcpu     = 2

  cpu = {
    mode = "host-passthrough"
  }

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.centos8-qcow2[each.key].id
  }

  cloudinit = libvirt_cloudinit_disk.commoninit[each.key].id

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

  connection {
    type        = "ssh"
    private_key = "${path.module}/../.ssh/${var.private_key}"
    user        = var.terraform_user
    host        = self.network_interface.0.addresses[0]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo dnf -y install python"
    ]
    on_failure = continue
  }

  provisioner "local-exec" {
    command = "echo job done!"
  }
}
