# Terraform libvirt presentation

Présentation concernant terraform et libvirt  pour Linux Meetup Montréal. Sept 1, 2020

## Installer Terraform

Ce demo utilise deux versions de terraform, soit les versions 0.12.29, et la version la plus récente, la 0.13.1

- Ils sont les deux téléchargeables de https://terraform.io. Pour les différencier, je les nomme `terraform12` et `terraform13`.


- le provider `libvirt` est disponible au https://github.com/dmacvicar/terraform-provider-libvirt

- il faut le le télécharger et installer selon les instructions donnés dans la description.

- pour le project demo3, qui nécessite terraform 0.13, il faut aussi l'installer dans un répertoire `.terraform`:

  - il faut copier `.terraform/plugins/registry.terraform.io/dmacvicar/libvirt` à `.terraform/plugins/registry.terraform.io/hashicorp/libvirt`

## Installer Docker

Installez Docker selon votre système, avec Docker swarm. Assurez-vous que docker fonctionne, et que swarm est initialisé.

## Installer les préalables

- Dans la racine du projet, il faut un répertoire `.ssh`, qui contient une clé ssh pour accéder aux machines virtuelles qu'on va créer.

  ```
  ..> ssh-keygen -t ed25519 -f ${project_dir}/.ssh/id_ed25519
  ```

- Il faut avoir avoir un endroit ou emmagasiner les images qu'on va utiliser: (changer-les au besoin), et pour le pool de storage temporaire.

```
	cd ${project_dir}
	mkdir -p .lib/libvirt/images
	wget -P .lib/libvirt/images http://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2
```

- ensuite, ajuster le contenu de `exports` au besoin, et

```
  ..> source exports
```
