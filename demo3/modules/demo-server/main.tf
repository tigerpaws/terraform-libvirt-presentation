#

# variables
variable "image_repo" {}
variable "machine_name" {}
variable "network_address" {}
variable "pool_name" {}
variable "private_key" {}
variable "public_key" {}
variable "domain" {}
variable "terraform_user" { default = "tf" }

#

resource "libvirt_volume" "centos8-qcow2" {
  name = "centos8.${var.machine_name}.qcow2"
  #source = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
  source = "${var.image_repo}/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
  format = "qcow2"
  pool   = var.pool_name
}

data "template_file" "index_page" {
  template = file("${path.module}/web-server/index.html")
  vars = {
    hostname = var.machine_name
  }
}

data "template_file" "user_data" {
  template = "${file("${path.module}/cloud-init.cfg")}"
  vars = {
    hostname    = var.machine_name
    fqdn        = "${var.machine_name}.3sz.ca"
    public_key  = var.public_key
    index_page  = base64encode(data.template_file.index_page.rendered)
    http_server = filebase64("${path.module}/web-server/simplehttp.py")
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config.cfg")
  vars = {
    domain      = var.domain
    address     = var.network_address
    gateway     = cidrhost(var.network_address, 1)
    name_server = cidrhost(var.network_address, 1)
  }
}

# Use CloudInit to add the instance

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.machine_name}.init.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = var.pool_name
}

# Define KVM domain to create
resource "libvirt_domain" "vms" {
  name   = var.machine_name
  memory = "2048"
  vcpu   = 2

  cpu = {
    mode = "host-passthrough"
  }

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.centos8-qcow2.id
  }

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

  connection {
    type        = "ssh"
    private_key = "${path.root}/.ssh/${var.private_key}"
    user        = var.terraform_user
    host        = self.network_interface.0.addresses[0]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo dnf -y install python"
    ]
    on_failure = continue
  }

  provisioner "local-exec" {
    command = "echo job done!"
  }
}
