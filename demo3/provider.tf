
# Libvirt system
provider "libvirt" {
  uri = "qemu:///system"
}

# Libvirt user session
#provider "libvirt" {
#  uri = "qemu:///session"
#}

terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}
