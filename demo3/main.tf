# requires terraform 13

# variables

variable "web_servers" {}
variable "load_balancers" {}
variable "domain" {}

# generate web servers and load balancers

# create storage pool
resource "libvirt_pool" "demo3" {
  name = "demo3"
  type = "dir"
  path = "${var.pool_dir}/demo3"
}

module "demo_server" {
  source     = "./modules/demo-server"
  depends_on = [libvirt_pool.demo3]

  # make each server
  for_each = var.web_servers

  #set the variables
  machine_name    = "${each.value.name}"
  network_address = "${each.value.address}"
  pool_name       = libvirt_pool.demo3.name
  public_key      = file("${var.public_key}")
  private_key     = "${path.module}/../.ssh/${var.private_key}"
  domain          = var.domain
  image_repo      = var.image_repo
}

module "load_balancer" {
  source     = "./modules/load-balancer"
  depends_on = [libvirt_pool.demo3]

  # make each load_balancer
  for_each = var.load_balancers

  #set the variables
  machine_name    = "${each.value.name}"
  network_address = "${each.value.address}"
  pool_name       = libvirt_pool.demo3.name
  public_key      = file("${var.public_key}")
  private_key     = "${path.module}/../.ssh/${var.private_key}"
  domain          = var.domain
  image_repo      = var.image_repo
}

output "web_servers" {
  value = [
    for key, value in var.web_servers : {
      "name"      = key,
      "addresses" = module.demo_server["${key}"].addresses
    }
  ]
}

output "load_balancers" {
  value = [
    for key, value in var.load_balancers : {
      "name"    = key,
      "address" = module.load_balancer["${key}"].addresses
    }
  ]
}
