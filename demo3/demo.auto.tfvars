
domain = "3sz.ca"

web_servers = {
  vweb1 = {
    name    = "vweb1",
    address = "192.168.122.11/24"
  },
  vweb2 = {
    name    = "vweb2",
    address = "192.168.122.12/24"
  }
}

load_balancers = {
  vlb1 = {
    name    = "vlb1",
    address = "192.168.122.5/24"
  },
  vlb2 = {
    name    = "vlb2",
    address = "192.168.122.6/24"
  }
}
