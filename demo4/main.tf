
provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "httpd" {
  name = "httpd:2.4"
}

#prints the used contained ID for demonstration, optional
output "Docker_Image" {
  value = "Using ${docker_image.httpd.latest}"
}

resource "docker_container" "webserver" {
  name  = "webserver"
  image = docker_image.httpd.latest
  count = 1
  ports {
    internal = 80
    external = 8080
  }
}

output "connect_to" {
  value = "Please connect to port 8080 to reach the http server"
}
