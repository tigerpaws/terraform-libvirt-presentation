#
#

#resource "null_resource" "image_create_local" {
#  provisioner "local_exec" {
#    command = "docker image build -t picalc.git"
#  }
#}

resource "docker_image" "picalc_image" {
  name         = "picalc:git"
  keep_locally = true
  # depends_on = [null_resource.image_create_local] # if we want to recreate the image every time
}

resource "docker_service" "app-server" {
  name = "picalc-service"

  task_spec {
    restart_policy = {
      condition = "on-failure" # respawn continer if it fails
    }

    container_spec {
      image = docker_image.picalc_image.latest
    }

    resources {
      limits {
        nano_cpus    = 250000000 # ~ 25% load, 1*10^-9 cpu shares in ns
        memory_bytes = 10000000  # 100MB
      }
    }
  }

  mode {
    replicated {
      replicas = 4
    }
  }

  endpoint_spec {
    ports {
      target_port    = 80
      published_port = 8000
    }
  }
}

output "connect_to" {
  value = "Please connect to port 8000 to reach a picalc server"
}
